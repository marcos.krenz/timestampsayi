#!/usr/local/bin/node
const cors = require('cors');
const ejs = require('ejs').renderFile;
const bodyParser = require('body-parser');
const path = require('path');
const express = require('express');
const app = express();

const indexRoutes = require('./routes/index');

// settings
app.set('port', process.env.PORT || 3000);
app.engine('html', ejs);
app.set('view engine', 'ejs');

// middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// routes
app.use('/api', indexRoutes);

// static files
app.use(express.static(path.join(__dirname, 'dist')));

// start server
app.listen(app.get('port'), () =>
    console.log('Server on port', app.get('port'))
);
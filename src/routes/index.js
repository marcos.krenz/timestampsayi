const router = require('express').Router();
const OpenTimestamps = require('javascript-opentimestamps');
const json2plain = require('json2plain');
const sha256 = require('js-sha256');
const Notary = require('../../node_modules/javascript-opentimestamps/src/notary')

router.post('/stamp', (req, res, next) => {
    const detached = OpenTimestamps.DetachedTimestampFile.fromHash(new OpenTimestamps.Ops.OpSHA256(), req.body.hash);
    OpenTimestamps.stamp(detached).then(() => {
        fileOts = detached.serializeToBytes();
        res.send(JSON.stringify(fileOts));
    }).catch(e => res.send({error: e}));
});

router.post('/verify', (req, res, next) => {
    const fileOts = Buffer.from(req.body.otsHex,'hex');
    const detached = OpenTimestamps.DetachedTimestampFile.fromHash(new OpenTimestamps.Ops.OpSHA256(), req.body.hash);
    const detachedOts = OpenTimestamps.DetachedTimestampFile.deserialize(fileOts);
    OpenTimestamps.verify(detachedOts,detached).then(verifyResult => {
        res.send(verifyResult);
    }).catch(e => res.send({error: e}));
});

router.post('/info', (req, res, next) => {
    const fileOts = Buffer.from(req.body.otsHex,'hex');
    const objInfo = OpenTimestamps.json(fileOts);
    res.send(objInfo);
});

router.post('/upgrade', (req, res, next) => {
    const fileOts = Buffer.from(req.body.otsHex,'hex');
    const detached = OpenTimestamps.DetachedTimestampFile.deserialize(fileOts);
    OpenTimestamps.upgrade(detached).then((changed) => {
        if (changed) {
            const upgradedFileOts = detached.serializeToBytes();
            res.send(JSON.stringify(upgradedFileOts));
        } else {
            res.send(JSON.stringify('Timestamp not upgraded'));
        }
    }).catch(e => res.send({error: e}));
});

router.post('/upgrade-hexandhex', (req, res, next) => {
    const fileOts = Buffer.from(req.body.otsHex,'hex');
    const detached = OpenTimestamps.DetachedTimestampFile.deserialize(fileOts);

    OpenTimestamps.upgrade(detached).then((changed) => {
        if (changed) {
            const upgradedFileOts = detached.serializeToBytes();
            detached.timestamp.directlyVerified().forEach(subStamp => {
                subStamp.attestations.forEach(attestation => {
                    if (attestation instanceof Notary.PendingAttestation) {
                        // check if already resolved
                        if (subStamp.isTimestampComplete()) {
                            res.send(JSON.stringify({ 
                                hexCertificado: buf2hex(upgradedFileOts),
                                completo: true 
                            }));
                        } else {
                            res.send(JSON.stringify({ 
                                hexCertificado: buf2hex(upgradedFileOts),
                                completo: false
                            }));
                        }
                    }
                })
            });
        } else {
            res.send(JSON.stringify({ msg: 'Timestamp not upgraded' }));
        }
    }).catch(e => res.send({error: e}));
});

router.post('/stamp-jsontoplain', (req, res, next) => {

    const textplain = json2plain(req.body, {indent: ' '});
    const array = strToByte(textplain);
    const hash = sha256.array(array);
    const hexArchivo = buf2hex(array);

    const detached = OpenTimestamps.DetachedTimestampFile.fromHash(new OpenTimestamps.Ops.OpSHA256(), hash);
    OpenTimestamps.stamp(detached).then(() => {

        fileOts = detached.serializeToBytes();

        res.send(JSON.stringify({
            hexArchivo: hexArchivo,
            hexCertificado: buf2hex(fileOts)
        }));

    }).catch(e => res.send({error: e}));
});

function strToByte(str) {
    const bytes = [];
    for (let i = 0; i < str.length; i++) {
        const char = str.charCodeAt(i);
        bytes.push(char & 0xFF);
    }
    return bytes;
}

function buf2hex(arrayBuffer) {
    return Array.prototype.map.call(new Uint8Array(arrayBuffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

module.exports = router;

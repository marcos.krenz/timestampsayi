import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'client';

  constructor(private router: Router) { }

  scroll(el) {
    this.router.navigate(['/']);
    el.scrollIntoView({behavior: 'smooth'});
  }

  navegarA(url) {
    window.location.href = url;
  }
}

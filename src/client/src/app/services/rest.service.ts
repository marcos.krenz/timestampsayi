import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class RestService {

  url = 'http://blockchain.ayi-asociados.com/api/';

  constructor(private http: HttpClient) { }

  private getHeaders(): HttpHeaders {
    const headers = new HttpHeaders().append('Content-Type', 'application/json');
    return headers;
  }

  stamp(obj) {
    const headers = this.getHeaders();
    return this.http.post(this.url + 'stamp', obj, {headers: headers, responseType: 'json'})
      .pipe(map(res => res));
  }

  verify(obj) {
    const headers = this.getHeaders();
    return this.http.post(this.url + 'verify', obj, {headers: headers, responseType: 'json'})
      .pipe(map(res => res));
  }

  info(obj) {
    const headers = this.getHeaders();
    return this.http.post(this.url + 'info', obj, {headers: headers, responseType: 'json'})
      .pipe(map(res => res));
  }

  upgrade(obj) {
    const headers = this.getHeaders();
    return this.http.post(this.url + 'upgrade', obj, {headers: headers, responseType: 'json'})
      .pipe(map(res => res));
  }

}

import { Directive, EventEmitter, HostListener, Output, Input } from '@angular/core';

@Directive({
  selector: '[appDropFile]'
})
export class DropFileDirective {

  @Input() dropFileActivado: boolean;
  @Output() file: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  @HostListener('dragover', ['$event']) public onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
  }

  @HostListener('drop', ['$event']) public onDrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();

    if (this.dropFileActivado) {
      const files = evt.dataTransfer.files;

      if (files.length === 1) {
        if (files[0].size < 25000000) {
          this.file.emit(files[0]);
        } else {
          this.file.emit('Archivo de tamaño mayor a 25MB');
        }
      } else {
        this.file.emit('Agregue de a un archivo');
      }
    }
  }
}

import { Component } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { saveAs } from 'file-saver/FileSaver';
import * as sha256 from 'js-sha256';
import { Router } from '@angular/router';

@Component({
  selector: 'app-stamp',
  templateUrl: './stamp.component.html',
  styleUrls: ['./stamp.component.css']
})

export class StampComponent {

  otsOrFileName;
  otsName;
  otsHex;
  otsCargado;
  estampando;
  valorEstampado = '';
  sizeFile;
  sizeOriginalFile;
  attested;
  verificando;
  valorVerificado;
  aviso1 = null;
  aviso2 = null;
  estadoDropzone1 = true;
  estadoDropzone2 = true;

  constructor(private restService: RestService,
              private router: Router) { }

  inputFile(file: any, primerInput): void {

    const files = file.path[0].files;

    if (files.length === 1) {
      if (files[0].size < 25000000) {
        if (primerInput) {
          this.uploadOtsOrFile(files[0]);
        } else {
          this.uploadFile(files[0]);
        }
      } else {
        if (primerInput) {
          this.uploadOtsOrFile('Archivo de tamaño mayor a 25MB');
        } else {
          this.uploadFile('Archivo de tamaño mayor a 25MB');
        }
      }
    } else {
      if (primerInput) {
        this.uploadOtsOrFile('Agregue de a un archivo');
      } else {
        this.uploadFile('Agregue de a un archivo');
      }
    }
  }

  uploadOtsOrFile(file: any): void {
    this.aviso1 = null;
    if (typeof file === 'string') {
      this.aviso1 = file;
      return;
    }

    this.otsName = null;
    this.otsOrFileName = file.name;
    this.sizeFile = this.humanFileSize(file.size);
    this.estampando = true;
    this.otsCargado = false;
    this.estadoDropzone1 = false;

    const reader = new FileReader();

    reader.onload = () => {

      const esOts = file.name.match(/\.[0-9a-z]+$/i);
      if (esOts !== null && esOts.length > 0 && esOts[0] === '.ots') {
        this.valorEstampado = '';
        const arrayBuffer = reader.result;
        this.otsHex = this.buf2hex(arrayBuffer);
        this.valorEstampado = 'ACTUALIZANDO ARCHIVO .OTS...';
        this.restService.upgrade({ otsHex: this.otsHex })
        .subscribe(res => {
          if (res.hasOwnProperty('error')) {
            this.estadoDropzone1 = true;
            this.valorEstampado = 'HA OCURRIDO UN ERROR, INTENTE MAS TARDE!';
            throw res;
          }
          this.otsCargado = true;
          if (res !== 'Timestamp not upgraded') {
            this.valorEstampado = '.OTS SE HA ACTUALIZADO, SE COMENZARÁ SU DESCARGA Y SE UTILIZARA PARA EL SIGUIENTE PASO';
            const arr = Object.keys(res).map(function(k) { return res[k]; });
            const ui8a = new Uint8Array(arr);
            const blob = new Blob([ui8a], {type: 'application/octet-stream'});
            saveAs(blob, this.otsOrFileName);
            this.otsHex = this.buf2hex(arr);
          } else {
            this.valorEstampado = '.OTS YA SE ENCUENTRA ACTUALIZADO';
          }
          this.estadoDropzone1 = true;
        }, err => {
          this.estadoDropzone1 = true;
          this.valorEstampado = 'NUESTROS SERVICIOS SE ENCUENTRAN EN MANTENIMIENTO, INTENTE MAS TARDE!';
          throw err;
        });
      } else {
        this.valorEstampado = 'ESTAMPANDO...';
        const arrayBuffer = reader.result;
        const hash = sha256.sha256.array(arrayBuffer);

        this.estampando = true;

        this.restService.stamp({ hash: hash })
          .subscribe(res => {
            if (res.hasOwnProperty('error')) {
              this.estadoDropzone1 = true;
              this.valorEstampado = 'NUESTROS SERVICIOS SE ENCUENTRAN EN MANTENIMIENTO, INTENTE MAS TARDE!';
              throw res;
            }
            const arr = Object.keys(res).map(function(k) { return res[k]; });
            const ui8a = new Uint8Array(arr);
            const blob = new Blob([ui8a], {type: 'application/octet-stream'});
            saveAs(blob, this.otsOrFileName + '.ots');
            this.valorEstampado = 'EXITO!';
            this.estadoDropzone1 = true;
          }, err => {
            this.valorEstampado = 'NUESTROS SERVICIOS SE ENCUENTRAN EN MANTENIMIENTO, INTENTE MAS TARDE!';
            this.estadoDropzone1 = true;
            throw err;
          }
        );
      }

    };
    reader.readAsArrayBuffer(file);
  }

  uploadFile(file: any): void {
    this.aviso2 = null;
    if (typeof file === 'string') {
      this.aviso2 = file;
      return;
    }

    this.otsName = file.name;
    this.sizeOriginalFile = this.humanFileSize(file.size);
    this.verificando = false;
    this.valorVerificado = 'VERIFICANDO...';
    this.estadoDropzone2 = false;
    const reader = new FileReader();

    reader.onload = () => {
      this.attested = null;
      const arrayBuffer = reader.result;
      const hash = sha256.sha256.array(arrayBuffer);
      this.verificando = true;
      this.restService.verify({ hash: hash, otsHex: this.otsHex })
        .subscribe(res => {
          if (res.hasOwnProperty('error')) {
            this.estadoDropzone1 = true;
            this.valorEstampado = 'HA OCURRIDO UN ERROR, INTENTE MAS TARDE!';
            throw res;
          }
          if (Object.keys(res).length !== 0) {
            this.attested = res;
            this.valorVerificado = 'EXITO!';
          } else {
            this.valorVerificado = 'ATESTACIÓN PENDIENTE';
          }
          this.estadoDropzone2 = true;
        }, err => {
          this.estadoDropzone2 = true;
          this.valorEstampado = 'HA OCURRIDO UN ERROR, INTENTE MAS TARDE!';
          throw err;
        });
    };
    reader.readAsArrayBuffer(file);
  }

  info() {
    this.router.navigate([`/info/${this.otsHex}`]);
  }

  buf2hex(arrayBuffer) {
    return Array.prototype.map.call(new Uint8Array(arrayBuffer), x => ('00' + x.toString(16)).slice(-2)).join('');
  }

  humanFileSize(bytes, si = true) {
    const thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
      return bytes + ' B';
    }
    const units = si
      ? ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
      : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    do {
      bytes /= thresh;
      ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + ' ' + units[u];
  }

  secondsToDate(seconds) {
    const curdate = new Date(null);
    curdate.setTime(seconds * 1000);
    return curdate.toLocaleString();
  }
}

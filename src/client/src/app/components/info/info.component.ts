import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { RestService } from '../../services/rest.service';
import { ActivatedRoute } from '@angular/router';

interface Timestamp {
  hash: string;
  op: string;
  result: string;
  timestamp: string;
}

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})

export class InfoComponent implements OnInit {

  timestamp;
  container;

  constructor(private restService: RestService,
              private route: ActivatedRoute) { }

  ngOnInit() {

    const otsHex = this.route.snapshot.params['ots'];

    this.restService.info({ otsHex: otsHex })
    .subscribe((res: Timestamp) => {
      $('#hash').html(res.hash);
      $('#digest').html(res.hash);
      $('#type').html(res.op);
      $('#title_digest').html(res.hash.substring(0, 12));
      this.print(res.timestamp);
    }, err => {
      console.log(err);
    });
  }

  // Print timestamp
  print(timestamp) {

    if (this.container === undefined) {
      this.container = $('#table');
    }

    if (timestamp.attestations !== undefined ) {
      timestamp.attestations.forEach((item) => {
        let div = this.printAttestation(item, item.fork);
        $(this.container).append(div);
        if (item.merkle !== undefined) {
          div = this.printMerkle(item.merkle, item.fork);
          $(this.container).append(div);
        }
      });
    }

    if (timestamp.tx !== undefined ) {
      const div = this.printTx(timestamp.tx, timestamp.ops[0].fork);
      $(this.container).append(div);
    }

    if (timestamp.ops === undefined ) {
      return;
    }

    if (timestamp.ops.length > 1) {
      const subdiv = this.printFork(timestamp.fork, timestamp.ops.length);
      $(this.container).append(subdiv);

      const div = document.createElement('div');
      $(div).addClass('table-i');
      $(div).append(`<div class='table'></div>`);
      $(div).appendTo(this.container);
      this.container = $(div).find('div');

    }

    if (timestamp.ops.length > 0) {
      timestamp.ops.forEach((item) => {
        const div = this.printTimestamp(item.op, item.arg, item.result, item.fork);
        $(this.container).append(div);
        this.print(item.timestamp);
      });
    }
  }

  printAttestation (item, fork) {
    const div = document.createElement('div');
    $(div).addClass('table-i');

    let title = 'Attestation';
    let color = 'grey';
    let content = '';
    if (item.type === 'BitcoinBlockHeaderAttestation') {
      title = 'Bitcoin Attestation';
       content = `Merkle root of Bitcoin block
        <strong class='hash' style='display: inline;'>${item.param}</strong>
        <a class='copy'></a>
        </div>`;
      color = 'green';
    } else if (item.type === 'LitecoinBlockHeaderAttestation') {
      title = 'Litecoin Attestation';
      content = `Merkle root of Litecoin block
        <strong class='hash' style='display: inline;'>${item.param}</strong>
        <a class='copy'></a>
        </div>`;
      color = 'gold';
    } else if (item.type === 'EthereumBlockHeaderAttestation') {
      title = 'Ethereum Attestation';
      content = `Merkle root of Ethereum block
        <strong class='hash' style='display: inline;'>${item.param}</strong>
        <a class='copy'></a>
        </div>`;
      color = 'gold';
    } else if (item.type === 'PendingAttestation') {
      title = 'Pending Attestation';
      content = `Pending attestation: server <a href='${item.param}' target="_blank">${item.param}</a>`;
      color = 'gold';
    } else if (item.type === 'UnknownAttestation') {
      title = 'Unknown attestation';
      content = `Unknown Attestation: payload <a href=''>${item.param}</a>`;
      color = 'grey';
    }

    const first = document.createElement('div');
    $(first).addClass('table-name ' + color);
    $(first).html(title);
    $(first).appendTo(div);

    const second = document.createElement('div');
    $(second).addClass('table-value table-value_copy');
    $(second).append(content);
    $(second).appendTo(div);

    return div;
  }

  printMerkle (merkle, fork) {
    const div = document.createElement('div');
    $(div).addClass('table-i');

    const title = 'Merkle Root';
    const content = merkle;
    const color = 'purple';

    const first = document.createElement('div');
    $(first).addClass(`table-name ${color}`);
    $(first).html(title);
    $(first).appendTo(div);

    const second = document.createElement('div');
    $(second).addClass('table-value table-value_copy');
    $(second).append(`<div class='badge'></div>`);
    if (fork > 0) {
      $(second).find('.badge').append(`<p class='step'>${fork}</p>`);
    }
    $(second).find('.badge').append(`<p class='hash'>${content}</p>`);
    $(second).find('.badge').append(`<a class='copy'></a>`);
    $(second).appendTo(div);

    return div;
  }

  printTx (tx, fork) {
    const div = document.createElement('div');
    $(div).addClass('table-i');

    const title = 'Parse TX';
    const content = tx;
    const color = 'purple';

    const first = document.createElement('div');
    $(first).addClass(`table-name ${color}`);
    $(first).html(title);
    $(first).appendTo(div);

    const second = document.createElement('div');
    $(second).addClass('table-value table-value_copy');
    $(second).append('<p>Transaction</p>');
    $(second).append(`<div class='badge'></div>`);

    if (fork > 0 ) {
      $(second).find('.badge').append(`<p class='step'>${fork}</p>`);
    }
    $(second).find('.badge').append(`<p class='hash'>${content}</p>`);
    $(second).find('.badge').append(`<a class='copy'></a>`);
    $(second).appendTo(div);

    return div;
  }

  printFork (fork, totfork) {
    const div = document.createElement('div');
    $(div).addClass('table-i');

    const title = 'Fork';
    const content = `Fork in ${totfork} paths`;
    const color = 'blue';

    const first = document.createElement('div');
    $(first).addClass('table-name ' + color);
    $(first).html(title);
    $(first).appendTo(div);


    const second = document.createElement('div');
    $(second).addClass('table-value');
    if (fork > 0) {
      $(second).append(`<p class='step'>${fork}</p>`);
    }
    $(second).append(`<p class=''>${content}</p>`);
    $(second).appendTo(div);

    return div;
  }

  printTimestamp (op, arg, result, fork) {
    const div = document.createElement('div');
    $(div).addClass('table-i');

    let content = result;
    if (arg.length > 0) {
      const start = content.indexOf(arg);
      const end = start + arg.length;
      content = `${result.substring(0, start)}<span class='green'>${arg}</span>${result.substring(end, result.length)}`;
    }
    const title = `${op}(${((arg.length > 0) ? arg.substring(0, 6) + '...' : '')})`;
    const color = 'purple';

    const first = document.createElement('div');
    $(first).addClass('table-name ');
    $(first).html(title);
    $(first).appendTo(div);

    const second = document.createElement('div');
    $(second).addClass('table-value');
    $(second).append(`<div class='badge'></div>`);
    if (fork > 0) {
      $(second).find('.badge').append(`<p class='step'>${fork}</p>`);
    }
    $(second).find('.badge').append(`<p class='hash'>${content}</p>`);
    $(second).appendTo(div);

    return div;
  }

}

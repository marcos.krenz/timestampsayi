import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StampComponent } from '../components/stamp/stamp.component';
import { InfoComponent } from '../components/info/info.component';

const rutas: Routes = [
  {path: 'info/:ots', component: InfoComponent},
  {path: '', component: StampComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(rutas)
  ],
  exports: [RouterModule],
  declarations: []
})

export class RuteoModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RuteoModule } from './routes/ruteo.module';

import { AppComponent } from './app.component';
import { StampComponent } from './components/stamp/stamp.component';
import { DropFileDirective } from './directives/drop-file.directive';
import { RestService } from './services/rest.service';
import { HttpClientModule } from '@angular/common/http';
import { InfoComponent } from './components/info/info.component';

@NgModule({
  declarations: [
    AppComponent,
    StampComponent,
    DropFileDirective,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    RuteoModule,
    HttpClientModule
  ],
  providers: [
    RestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
